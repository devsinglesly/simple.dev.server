<?php
/** @var \SimpleDev\Users\Event\CreateFromBidEvent $event */
?>
<div style="max-width: 640px;">
    <h3>Добрый день!</h3>
    <p>

            <?= $event->user->getFirstName(); ?>, для вас мы сформировали подробную инструкцию и условия для обучения.
    </p>
    <hr>
    <h2>Ваше направление - Web Development</h2>

    <h3>Стек</h3>
    <p>Для вас предпочтительно будет начать именно с перечисленных технологий.</p>

    <p><strong>PHP - </strong>Самый востребованный язык для Back-end разработки на текущий момент и очень простой в изучении</p>
    <p><strong>Реляционные базы данных - </strong>главная ценность любой компании это база данных. Реляционные БД занимают
        первое место в разработке самого надежного и дорогостоящего ПО.</p>
    <p><strong>Linux - </strong>99% серверов по всему миру используют операционную систему Linux. Знание и умение работать
        с данной системой является неотъемлемой частью каждого программиста.</p>
    <p><strong>JavaScript - </strong>самый универсальный и простой язык для быстрого создания пользовательского интерфейса</p>

    <br>

    <h3>Формат обучения</h3>
    <p>
        Наша методика обучения очень плодотворно отражается учениках, все кто не сдается и идет до конца в конечном счете
        получает полноценную удаленную работу, либо место в IT - Компании своего города.
    </p>

    <ul style="font-weight: bold">
        <li>Индвидуальный наставник</li>
        <li>Регулярные и персонализированные задания</li>
        <li>Структурированная информация</li>
        <li>Постоянные голосовые конференции</li>
        <li>Группа единомышленников</li>
        <li>Актуальные решения</li>
    </ul>


    <br>

    <h3>Стоимость и инструкция оплаты</h3>
    <p>
        Для вас сформированна стоимость ежемесячного обучения в размере: <strong><?= $event->price; ?></strong> руб/мес.
    </p>
    <p>
        Способы оплаты.
    </p>
    <ol style="margin-left: 15px; padding: 0;">
        <li><strong>Карта Сбербанк</strong></li>
        <ul style="margin-left: 10px; padding: 0;">
            <li>
                <div style="display: inline-block">Номер карты: </div>
                <div style="display: inline-block">4276 4500 1399 7131</div>
            </li>
            <li>
                <div style="display: inline-block">Получатель: </div>
                <div style="display: inline-block">АРТЕМ АЛЕКСАНДРОВИЧ И.</div>
            </li>
            <li>
                <div style="display: inline-block">Сообщение получателю: </div>
                <div style="display: inline-block">На обучение</div>
            </li>
        </ul>
        <li><strong>Лицевой счет</strong></li>

        <ul style="margin-left: 10px; padding: 0;">
            <li>
                <div style="display: inline-block">Номер счета: </div>
                <div style="display: inline-block">408 17 810 2 45002806596</div>
            </li>
            <li>
                <div style="display: inline-block">Фамилия: </div>
                <div style="display: inline-block">Ильиных</div>
            </li>
            <li>
                <div style="display: inline-block">Имя: </div>
                <div style="display: inline-block">Артем</div>
            </li>
            <li>
                <div style="display: inline-block">Отчество: </div>
                <div style="display: inline-block">Александрович</div>
            </li>
            <li>
                <div style="display: inline-block">Наименование банка: </div>
                <div style="display: inline-block">ОМСКОЕ ОТДЕЛЕНИЕ N 8634 ПАО СБЕРБАНК</div>
            </li>
            <li>
                <div style="display: inline-block">БИК: </div>
                <div style="display: inline-block">045209673</div>
            </li>
            <li>
                <div style="display: inline-block">Корр. счет: </div>
                <div style="display: inline-block">30101810900000000673</div>
            </li>
            <li>
                <div style="display: inline-block">Назначение платежа: </div>
                <div style="display: inline-block">На обучение</div>
            </li>
        </ul>
    </ol>

    <br>
    <h3>Приступайте к обучению</h3>
    <p>Как связаться с индвидуальным наставником и получать материалы</p>
    <ol>
        <li>Установите <a href="https://discordapp.com/">Discord</a></li>
        <li>Заходите в <a href="https://discord.gg/7CbKQCB">Группу</a></li>
        <li>Напишите <u>ментору выделенному синим цветом</u>, что вы оплатили и сообщите ваш id - <?= $event->user->getId(); ?></li>
        <li>Вас зарегистрируют в платформе и предоставят доступ к материалам</li>
        <li>Так же вы можете общаться с наставником по любым вопросам</li>
    </ol>
</div>


<?php

namespace App\Dto;


use App\Exception\RuntimeException;

class Email
{
    private $value;

    public function __construct(string $value)
    {
        $email = filter_var($value, FILTER_VALIDATE_EMAIL, ['default' => null]);

        if(!$email)
            throw new RuntimeException("Не корректный email");

        $this->value = $email;
    }

    public function get(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->get();
    }
}
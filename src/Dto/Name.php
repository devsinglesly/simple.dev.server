<?php

namespace App\Dto;


class Name
{
    private $first, $last, $middle;


    public function __construct(string $last, string $first, string $middle = '')
    {
        $this->last = $last;
        $this->first = $first;
        $this->middle = $middle;
    }

    /**
     * @return string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * @return string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    /**
     * @return string
     */
    public function getMiddle(): string
    {
        return $this->middle;
    }
}
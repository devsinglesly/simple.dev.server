<?php


namespace SimpleDev\Users\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use SimpleDev\Users\Model\Phone;

class PhoneRepository extends ServiceEntityRepository implements PhoneRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Phone::class);
    }
}
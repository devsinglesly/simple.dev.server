<?php

namespace SimpleDev\Courses\Event\Lesson;


use SimpleDev\Courses\Event\Event;
use SimpleDev\Courses\Model\Lesson;

class LessonCreateEvent extends Event
{
    public $lesson;

    public function __construct(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }
}
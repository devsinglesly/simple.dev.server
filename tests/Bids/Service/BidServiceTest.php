<?php
declare(strict_types=1);

namespace App\Tests\Bids\Service;

use SimpleDev\Bids\Repository\DummyBidRepository;
use App\Dto\Email;
use App\Dto\Name;
use App\Dto\Phone;
use PHPUnit\Framework\TestCase;
use SimpleDev\Bids\Repository\BidRepositoryInterface;
use SimpleDev\Bids\Service\BidService;

final class BidServiceTest extends TestCase
{
    /** @var BidRepositoryInterface */
    private $repository;

    public function setup(): void
    {
        $this->repository = new DummyBidRepository();
    }

    /**
     * @throws \Exception
     */
    public function testCreate(): void
    {
        $service = new BidService($this->repository);
        $bid = $service->create(new Name("Last", "First"),
            18,
            new Phone("+7 (999) 458 99-99"),
            new Email("some@gmail.com"),
            "status 1",
            "about");

        $this->assertIsInt($bid->getId());
        $this->assertEquals("Last", $bid->getLastName());
        $this->assertEquals("First", $bid->getFirstName());
        $this->assertEquals("some@gmail.com", $bid->getEmail());
        $this->assertEquals("+7 (999) 458 99-99", $bid->getPhone());
        $this->assertEquals(18, $bid->getAge());
        $this->assertEquals("status 1", $bid->getStatus());
        $this->assertEquals("about", $bid->getInfo());
    }
}
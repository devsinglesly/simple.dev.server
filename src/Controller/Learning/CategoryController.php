<?php

namespace App\Controller\Learning;


use SimpleDev\Learning\Model\Category;
use SimpleDev\Learning\Service\CategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @package App\Controller\Learning
 * @Route("/category")
 */
class CategoryController extends AbstractController
{

    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $order = $request->query->get('order') ?: 'DESC';
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;

        /** @var Category[] $categories */
        $categories = $this->categoryService->getAll([], ['id' => $order], $limit, $offset);

        return $this->json($categories);
    }



    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/published", methods={"GET"})
     */
    public function getPublish(Request $request): JsonResponse
    {
        $order = $request->query->get('order') ?: 'DESC';
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;

        /** @var Category[] $categories */
        $categories = $this->categoryService->getAll(['isPublished'=> true], ['id' => $order], $limit, $offset);

        return $this->json($categories);
    }

    /**
     * @Route("/", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function new(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $category = $this->categoryService->create(
            $data['title'],
            $data['description'],
            $data['preview']
        );

        return $this->json($category);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="\d+"})
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $category = $this->categoryService->update($id,
            $data['title'],
            $data['description'],
            $data['preview']
        );

        return $this->json($category);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @Route("/{id}/publish", methods={"PATCH"}, requirements={"id"="\d+"})
     */
    public function publish(Request $request, int $id): JsonResponse
    {
        $category = $this->categoryService->publish($id);

        return $this->json($category);
    }

    /**
     * @Route("/{id}/image", methods={"PATCH"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function image(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $category = $this->categoryService->image($id, $data['path']);

        return $this->json($category);
    }

    /**
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="\d+"})
     * @param int $id
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {
        $this->categoryService->remove($id);

        return $this->json(true);
    }
}
<?php

namespace SimpleDev\Courses\Event;


trait EventTrait
{
    /**
     * @var Event[]
     */
    private $events = [];

    public function recordEvent(Event $event)
    {
        $this->events[] = $event;
    }

    public function releaseEvents()
    {
        $events = $this->events;
        $this->events = [];
        return $events;
    }
}
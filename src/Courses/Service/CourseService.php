<?php

namespace SimpleDev\Courses\Service;


use SimpleDev\Courses\DTO\Price;
use SimpleDev\Courses\Event\EventDispatcherInterface;
use SimpleDev\Courses\Model\Course;
use SimpleDev\Courses\Repository\CourseRepositoryInterface;

class CourseService
{
    private $courseRepository;

    private $eventDispatcher;

    public function __construct(CourseRepositoryInterface $courseRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->courseRepository = $courseRepository;
    }

    /**
     * @param int $id
     * @return Course
     */
    public function getById(int $id) : Course
    {
        return $this->courseRepository->findById($id);
    }

    /**
     * @return Course[]
     */
    public function getPublished() : array
    {
        return $this->courseRepository->findByParams(['publish' => true]);
    }

    /**
     * @return Course[]
     */
    public function getAll() : array
    {
        return $this->courseRepository->findAll();
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $preview
     * @param Price $price
     * @return Course
     */
    public function create(string $title, string $description, string $preview, Price $price) : Course
    {
        $course = Course::create($title, $description, $preview, $price);
        $this->courseRepository->save($course);

        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $course;
    }

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param string $preview
     * @return Course
     */
    public function edit(int $id, string $title, string $description, string $preview) : Course
    {
        /** @var Course $course */
        $course = $this->courseRepository->findById($id);
        $course->edit($title, $description, $preview);
        $this->courseRepository->update($course);

        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $course;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id) : bool
    {
        $course = $this->courseRepository->findById($id);
        $status = $this->courseRepository->remove($course);

        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $status;
    }

    /**
     * @param int $id
     * @param Price $price
     * @return Course
     */
    public function changePrice(int $id, Price $price) : Course
    {
        $course = $this->courseRepository->findById($id);
        $course->changePrice($price);
        $this->courseRepository->update($course);

        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $course;
    }

    /**
     * @param int $id
     * @return Course
     */
    public function publish(int $id): Course
    {
        $course = $this->courseRepository->findById($id);
        $course->publish();
        $this->courseRepository->update($course);
        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $course;
    }


    /**
     * @param int $id
     * @return Course
     */
    public function hide(int $id): Course
    {
        $course = $this->courseRepository->findById($id);
        $course->hide();
        $this->courseRepository->update($course);

        $this->eventDispatcher->dispatchAll($course->releaseEvents());

        return $course;
    }

}
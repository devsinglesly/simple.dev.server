<?php

namespace SimpleDev\Courses\Repository;


use SimpleDev\Courses\Model\Course;

interface CourseRepositoryInterface
{

    /**
     * @param array $criteria
     * @return Course[]
     */
    public function findByParams(array $criteria): array;

    /**
     * @return Course[]
     */
    public function findAll() : array;

    /**
     * @param int $id
     * @return Course
     */
    public function findById(int $id) : Course;

    /**
     * @param Course $course
     * @return Course
     */
    public function save(Course $course): Course;

    /**
     * @param Course $course
     * @return Course
     */
    public function update(Course $course): Course;

    /**
     * @return bool
     */
    public function remove(Course $course) : bool;
}
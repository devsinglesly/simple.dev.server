<?php

namespace SimpleDev\Users\Model;

use Doctrine\ORM\Mapping as ORM;

use SimpleDev\Users\Model\User as User;

/**
 * Class Phone
 * @package SimpleDev\Users\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Users\Repository\PhoneRepository")
 */
class Phone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phones")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    public function __construct(string $phone)
    {
        $this->number = $phone;
    }

    public function set(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

}
<?php

namespace SimpleDev\Users\Service;


use App\Auth\Token;
use App\Auth\TokenData;
use App\Exception\DomainException;
use SimpleDev\Users\Model\User;
use SimpleDev\Users\Repository\UserRepositoryInterface;
use SimpleDev\Users\ValueObject\Email;
use SimpleDev\Users\ValueObject\Password;

class AuthService
{
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $token
     * @return User
     * @throws \App\Auth\JsonWebTokenException
     */
    public function getUserByToken(string $token): User
    {
        $data = TokenData::get($token);
        $user = $this->userRepository->findOneByFields(['id' => $data['id']]);
        return $user;
    }

    /**
     * @param Email $email
     * @param Password $password
     * @return User
     */
    public function signup(Email $email, Password $password): User
    {
        $this->guardEmailAllowToSignup((string)$email);
        $user = User::signup($email, $password);
        $this->userRepository->save($user);

        return $user;
    }


    /**
     * @param string $email
     * @param string $password
     * @return User
     */
    public function login(string $email, string $password): User
    {
        $this->guardUserExists($email);
        /** @var User $user */
        $user = $this->userRepository->findOneByFields(['email' => $email]);
        $user->passwordVerify($password);

        return $user;
    }

    /**
     * @param string $token
     * @return User
     * @throws \App\Auth\JsonWebTokenException
     */
    public function check(string $token): User
    {
        $data = TokenData::get($token);

        $user = $this->userRepository->findOneByFields(['id' => $data['id']]);

        return $user;
    }

    public function createToken(User $user): Token
    {
        $token = Token::create([
            'id' => $user->getId(),
            'email' => $user->getEmail()
        ]);

        return $token;
    }

    /**
     * @param string $email
     */
    private function guardUserExists(string $email): void
    {
        if(!$this->userRepository->findOneByFields(['email' => $email]))
        {
            throw new DomainException("Пользователь не найден");
        }
    }

    /**
     * @param string $email
     */
    private function guardEmailAllowToSignup(string $email): void
    {
        if($this->userRepository->findOneByOrNull(['email' => $email]))
            throw new DomainException("Email уже занят");
    }
}
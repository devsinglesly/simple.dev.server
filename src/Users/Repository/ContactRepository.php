<?php

namespace SimpleDev\Users\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use SimpleDev\Users\Model\Contact;

class ContactRepository extends ServiceEntityRepository implements ContactRepositoryInterface
{
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Contact::class);
        $this->manager = $manager;
    }

    public function createEmpty(): Contact
    {
        $contact = new Contact();
        $this->manager->persist($contact);
        $this->manager->flush();
        return $contact;
    }
}
<?php

namespace App\Controller\Bids;


use App\Dto\Email;
use App\Dto\Name;
use App\Dto\Phone;
use SimpleDev\Bids\Model\Bid;
use SimpleDev\Bids\Service\BidService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BidController extends AbstractController
{

    private $bidService;

    public function __construct(BidService $bidService)
    {
        $this->bidService = $bidService;
    }

    /**
     * @Route("/", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $name = new Name($data['last_name'], $data['first_name']);
        $email = new Email($data['email']);
        $phone = new Phone($data['phone']);
        $info = $data['info'];
        $status = $data['status'];
        $age = $data['age'];

        $bid = $this->bidService->create($name, $age, $phone, $email, $status, $info);

        return $this->json($bid);
    }

    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $order = $request->query->get('order') ?? 'DESC';
        $limit = $request->query->has('limit') ? (int)$request->query->get('limit') : 10;
        $offset = $request->query->has('offset') ? (int)$request->query->get('offset') : 0;
        $status = $request->query->has('status') ? $request->query->get('status') : Bid::WAIT;

        return $this->json(
            $this->bidService->getAll(
                ['callStatus' => $status],
                ['id' => $order],
                $offset,
                $limit

            )
        );
    }

}
<?php

namespace SimpleDev\Courses\Event;


interface EventDispatcherInterface
{
    public function dispatch(Event $event) : void;

    /**
     * @param Event[] $events
     */
    public function dispatchAll(array $events) : void;
}
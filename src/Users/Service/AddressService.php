<?php

namespace SimpleDev\Users\Service;


use SimpleDev\Users\Model\Address;
use SimpleDev\Users\Repository\AddressRepositoryInterface;

class AddressService
{
    private $repository;

    public function __construct(AddressRepositoryInterface $addressRepository)
    {
        $this->repository = $addressRepository;
    }

    public function createEmpty(): Address
    {
        return $this->repository->save(new Address());
    }
}
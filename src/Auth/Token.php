<?php

namespace App\Auth;


class Token
{
    const ALG = 'SHA256';
    const TYPE = 'JWT';
    const DELIMITER = '.';
    const EXPIRE = 5;
    const SALT = "iuawd7AWD8g8yagDJNdawd8gAWdiupnu";


    /**
     * @var string|null
     */
    private $data = null;


    /**
     * Token constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function get(): string
    {
        $this->data['express_in'] = time();
        $this->data['express_out'] = $this->getOutTime();

        $token = implode(self::DELIMITER, [
            self::headers(),
            self::encode($this->data),
            self::signature($this->data)
        ]);

        return $token;
    }

    /**
     * @param array $data
     * @return Token
     */
    public static function create(array $data): Token
    {
        $token = new self($data);
        return $token;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->get();
    }

    public static function headers():string
    {
        return base64_encode(json_encode([
            'alg' => self::ALG,
            'type' => self::TYPE
        ]));
    }

    /**
     * @param array $data
     * @return string
     */
    public static function signature(array $data):string
    {
        return base64_encode(hash(self::ALG, serialize($data) . self::SALT));
    }

    public static function encode(array $data):string
    {
        return base64_encode(json_encode($data));
    }

    public static function decode(string $token): array
    {
        $data = json_decode(base64_decode($token), true);
        if($data == null)
        {
            throw new JsonWebTokenException("Incorrect token");
        }
        return $data;
    }


    /**
     * @return int
     */
    private static function getOutTime() : int
    {
        return time() + (self::EXPIRE * 60);
    }
}
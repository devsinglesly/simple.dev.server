Пользователи
    ** Регистрация по email
    ** Авторизация по email & password
    ** Добавление пользователя
    ** Изменить данные пользователя
    ** Изменить контакты пользователя
    ** Изменить адрес пользователя
    ** Назначить статус пользователя
    ** Добавить репозитори
    ** Удалить репозитори
    ** Добавить телефон
    ** Удалить телефон

*
    UsersController
    /api/users
              /         GET
              /auth     POST
              /signup   POST
              /create   POST
              /{user_id}/update   PUT // изменить информацию о пользователе имя фамилию и т.п
              /check    GET // проверить на авторизацию по токену
*
    AddressController
    /api/users/address
                      /{user_id} PUT изменить
*

*
    RepositoryController
    /api/users/repository
                      / POST // добавить репозитори
                      /{repository_id} DELETE // удалить репозитори
                      
                        
Статусы
    - Ученик
    - Наставник
    - Администратор
    
User
    last_name
    first_name
    middle_name
    age
    email
    password
    type
    
Repository ( many to one )
    link
    user_id
    
Address (one to one)
    address_country
    address_city
    address_street
    address_build
    address_flat
    user_id
    
Contact (one to one)
    contact_discord
    contact_vk
    contact_skype
    user_id

Phone ( many to one )
    phone
    contact_id
<?php

namespace SimpleDev\Users\ValueObject;


use App\Exception\DomainException;

final class Email extends ValueObject
{
    /**
     * @param string $value
     * @return string
     */
    protected function validate(string $value) : string
    {
        $pattern = "^.+\@.+\..+$";

        if(!preg_match("#{$pattern}#", $value))
        {
            throw new DomainException("Не корректный email");
        }

        return $value;
    }
}
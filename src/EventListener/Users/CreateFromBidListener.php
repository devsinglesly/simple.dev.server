<?php

namespace App\EventListener\Users;


use SimpleDev\Users\Event\CreateFromBidEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CreateFromBidListener
{

    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function handle(CreateFromBidEvent $event)
    {
        $message = new \Swift_Message('Инструкция по обучению');
        $message->setFrom('no-reply@simple-development.ru');
        $message->setTo($event->user->getEmail());

        ob_start();
        require_once __DIR__ . './../../../templates/mail/create.from.bid.html.php';
        $content = ob_get_clean();

        $message->setBody($content, "text/html");

        $this->mailer->send($message);

    }
}
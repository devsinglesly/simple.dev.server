<?php

namespace SimpleDev\Learning\Repository;


use SimpleDev\Learning\Model\Lesson;

interface LessonRepositoryInterface
{
    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function save(Lesson $lesson) : Lesson;

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return Lesson[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array;

    /**
     * @param int $id
     * @param array $criteria
     * @return Lesson
     */
    public function findOne(int $id, array $criteria): Lesson;

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function update(Lesson $lesson): Lesson;

    /**
     * @param Lesson $lesson
     * @return bool
     */
    public function remove(Lesson $lesson): bool;
}
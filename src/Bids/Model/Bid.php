<?php

namespace SimpleDev\Bids\Model;

use App\Dto\Email;
use App\Dto\Name;
use App\Dto\Phone;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;

/**
 * Class Bid
 * @package SimpleDev\Bids\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Bids\Repository\BidRepository")
 * @ORM\Table(name="bids_list")
 */
class Bid
{

    const ACCEPTED = 0;
    const REJECT = 1;
    const WAIT = 2;

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", nullable=false)
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", nullable=false)
     */
    private $firstName;

    /**
     * @var integer
     * @ORM\Column(name="age", type="integer", nullable=false)
     */
    private $age;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", nullable=false)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="phone", type="string", nullable=false)
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private $status;

    /**
     * @var string|null
     * @ORM\Column(name="info", type="string")
     */
    private $info;

    /**
     * @var int
     * @ORM\Column(name="call_status", type="smallint", nullable=false)
     */
    private $callStatus = self::WAIT;

    /**
     * @var DateTime
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;


    /**
     * @param Name $name
     * @param int $age
     * @param Email $email
     * @param Phone $phone
     * @param string $status
     * @param string|null $info
     * @return Bid
     * @throws \Exception
     */
    public static function create(Name $name, int $age, Email $email, Phone $phone, string $status, ?string $info): self
    {
        $bid = new self();
        $bid->lastName = $name->getLast();
        $bid->firstName = $name->getFirst();
        $bid->email = $email->get();
        $bid->age = $age;
        $bid->phone = $phone->get();
        $bid->status = $status;
        $bid->info = $info;
        $bid->timestamp = new DateTime();
        $bid->wait();
        return $bid;
    }

    public function wait(): self
    {
        $this->callStatus = self::WAIT;

        return $this;
    }

    public function reject(): self
    {
        $this->callStatus = self::REJECT;

        return $this;
    }

    public function accept() : self
    {
        $this->callStatus = self::ACCEPTED;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @return int
     */
    public function getCallStatus(): int
    {
        return $this->callStatus;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getDate(): string
    {
        return $this->timestamp->format('d.m.Y H:i');
    }
}
<?php

namespace SimpleDev\Users\ValueObject;


use App\Exception\DomainException;

final class Password extends ValueObject
{

    /**
     * @param string $value
     * @return string
     * @throws DomainException
     */
    protected function validate(string $value): string
    {
        if(strlen($value) < 5)
        {
            throw new DomainException("Пароль не меньше 5 символов");
        }

        return $value;
    }
}
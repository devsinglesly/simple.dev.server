<?php

namespace SimpleDev\Users\Repository;


use SimpleDev\Users\Model\User;

interface UserRepositoryInterface
{
    /**
     * @param array $criteria
     * @param array $order
     * @param int $offset
     * @param int $limit
     * @return User[]
     */
    public function findAllBy(array $criteria, array $order, int $offset, int $limit): array;

    /**
     * @param array $criteria
     * @return User|null
     */
    public function findOneByFields(array $criteria): ?User;

    /**
     * @param string $email
     * @return User
     */
    public function findOneByEmail(string $email): User;

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user) : User;


    public function update(User $user): User;

    /**
     * @param array $criteria
     * @return User|null
     */
    public function findOneByOrNull(array $criteria): ?User;
}
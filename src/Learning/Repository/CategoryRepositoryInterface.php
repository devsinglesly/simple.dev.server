<?php

namespace SimpleDev\Learning\Repository;


use SimpleDev\Learning\Model\Category;

interface CategoryRepositoryInterface
{
    /**
     * @param array $criteria - find parameters
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset) : array;

    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category;

    public function update(Category $category): Category;

    /**
     * @param Category $category
     * @return bool
     */
    public function remove(Category $category): bool;

    /**
     * @param int $id
     * @return Category
     */
    public function findOne(int $id): Category;
}
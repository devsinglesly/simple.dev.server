<?php

namespace SimpleDev\Learning\Service;


use SimpleDev\Learning\Model\Category;
use SimpleDev\Learning\Repository\CategoryRepositoryInterface;

class CategoryService
{
    private $repository;

    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string|null $preview
     * @return Category
     */
    public function create(string $title, string $description, string $preview = null): Category
    {
        $category = Category::create($title, $description, $preview);
        $this->repository->save($category);
        return $category;
    }

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param string $preview
     * @return Category
     */
    public function update(int $id, string $title, string $description, string $preview): Category
    {
        $category = $this->repository->findOne($id);

        $category->changeTitle($title);
        $category->changeDescription($description);
        $category->changePreview($preview);

        $this->repository->update($category);

        return $category;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function publish(int $id): Category
    {
        $category = $this->repository->findOne($id);

        $category->changePublished();

        $this->repository->update($category);

        return $category;
    }

    /**
     * @param int $id
     * @param string $path
     * @return Category
     */
    public function image(int $id, string $path): Category
    {
        $cat = $this->repository->findOne($id);
        $cat->changePreview($path);

        $this->repository->update($cat);

        return $cat;
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit = 20, int $offset = 0): array
    {
        $categories = $this->repository->getAll($criteria, $orderBy, $limit, $offset);

        return $categories;
    }

    public function getOne(int $id): Category
    {
        return $this->repository->findOne($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        $category = $this->repository->findOne($id);
        $this->repository->remove($category);
        return true;
    }
}
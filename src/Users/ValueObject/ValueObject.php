<?php

namespace SimpleDev\Users\ValueObject;


abstract class ValueObject
{
    /**
     * @var string
     */
    private $value;

    public function __construct(string $value)
    {
        $this->value = $this->validate($value);
    }

    public function __toString()
    {
        return $this->getValue();
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     */
    abstract protected function validate(string $value) : string;
}
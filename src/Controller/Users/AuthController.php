<?php

namespace App\Controller\Users;

use App\Auth\Token;
use App\Exception\AuthException;
use SimpleDev\Users\Service\AuthService;
use SimpleDev\Users\ValueObject\Email;
use SimpleDev\Users\ValueObject\Password;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package App\Controller\Users
 */
class AuthController extends AbstractController
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @Route("/auth", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $user = $this->authService->login(
            $data['email'],
            $data['password']
        );

        $token = $this->authService->createToken($user);

        return $this->json([
            'access_token' => (string)$token,
            'user_info' => $user
        ]);
    }


    /**
     * @Route("/signup", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function signup(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $user = $this->authService->signup(
            new Email($data['email']),
            new Password($data['password'])
        );

        $token = $this->authService->createToken($user);

        return $this->json([
            'access_token' => (string)$token,
            'user_info' => $user
        ]);
    }

    /**
     * @Route("/check", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Auth\JsonWebTokenException
     */
    public function check(Request $request): JsonResponse
    {
        $token = $request->headers->get('authorization');


        if(!$token)
            throw new AuthException("Не передан токен");

        $user = $this->authService->check($token);

        return $this->json($user);
    }
}
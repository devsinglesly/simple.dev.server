<?php

namespace App\Exception;


use Throwable;

class RuntimeException extends \RuntimeException implements OutableExceptionInterface
{
    public function __construct(string $message = "", int $code = 403, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
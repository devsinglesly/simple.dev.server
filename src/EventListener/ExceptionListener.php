<?php

namespace App\EventListener;

use App\Exception\NotFoundException;
use App\Exception\OutableExceptionInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     * @return GetResponseForExceptionEvent
     */
	public function onKernelException(GetResponseForExceptionEvent $event)
	{
		$exception = $event->getException();
		$event->allowCustomResponseCode();



		if($exception instanceof OutableExceptionInterface)
		{
            $response = new JsonResponse([
                'error' => $exception->getMessage()
            ]);
			$response->setStatusCode($exception->getCode());
            $response->headers->set('Content-Type', "application/json");
            $event->setResponse($response);
		}

		return $event;
	}
}
<?php

namespace SimpleDev\Courses\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Parent_;
use SimpleDev\Courses\Model\Course;


class CourseRepository extends ServiceEntityRepository implements CourseRepositoryInterface
{

    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Course::class);
        $this->manager = $manager;
    }

    /**
     * @param array $criteria
     * @return Course[]
     */
    public function findByParams(array $criteria): array
    {
        $courses = parent::findBy($criteria);

        return $courses;
    }


    /**
     * @return Course[]
     */
    public function findAll() : array
    {
        return parent::findAll();
    }

    /**
     * @param int $id
     * @return Course
     */
    public function findById(int $id): Course
    {
        /** @var Course|null $course */
        $course = $this->findOneBy(['id' => $id]);
        if(!$course){
            throw new NotFoundException("Курс не найден");
        }

        return $course;
    }

    /**
     * @param Course $course
     * @return Course
     */
    public function save(Course $course): Course
    {
        $this->manager->persist($course);
        $this->manager->flush();
        return $course;
    }

    /**
     * @param int $id
     * @param Course $course
     * @return Course
     */
    public function update(Course $course): Course
    {
        $this->manager->flush();
        return $course;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(Course $course): bool
    {
        $this->manager->remove($course);
        $this->manager->flush();
        return true;
    }
}
<?php

namespace SimpleDev\Users\ValueObject;


use App\Exception\DomainException;

class Phone extends ValueObject
{

    /**
     * @param string $value
     * @return string
     */
    protected function validate(string $value): string
    {
        $pattern = "^\+\d\s(\d{3})\s\d{3}\s\d{2}\-\d{2}$";

        if(!preg_match("#{$pattern}#", $value))
        {
            throw new DomainException("Не корректный номер");
        }

        return $value;

    }
}
<?php

namespace App\EventListener;


use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RequestListener
{
    public function onKernelRequest(GetResponseEvent $event): GetResponseEvent
    {
        if(function_exists('getallheaders')){
            $headers = getallheaders();

            if(isset($headers['HTTP_Authorization'])){
                $event->getRequest()->headers->set('Authorization', $headers['HTTP_Authorization']);
            }

        }

        return $event;
    }
}
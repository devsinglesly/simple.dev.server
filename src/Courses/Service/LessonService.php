<?php

namespace SimpleDev\Courses\Service;


use SimpleDev\Courses\Event\EventDispatcherInterface;
use SimpleDev\Courses\Model\Course;
use SimpleDev\Courses\Model\Lesson;
use SimpleDev\Courses\Repository\LessonRepositoryInterface;

class LessonService
{
    private $lessonRepository;

    private $eventDispatcher;

    public function __construct(LessonRepositoryInterface $lessonRepository, EventDispatcherInterface $eventDispatcher)
    {
        $this->lessonRepository = $lessonRepository;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param int $id
     * @return Lesson[]
     */
    public function getAllByCourseId(int $id) : array
    {
        return $this->lessonRepository->findByParams(['course' => $id]);
    }

    /**
     * @param int $id
     * @return Lesson
     */
    public function getById(int $id) : Lesson
    {
        return $this->lessonRepository->findById($id);
    }

    /**
     * @return Lesson[]
     */
    public function getAll() : array
    {
        return $this->lessonRepository->findAll();
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $attachment
     * @param string $preview
     * @param Course $course
     * @return Lesson
     */
    public function create(string $title, string $description, string $attachment, string $preview, Course $course): Lesson
    {
        $lesson = Lesson::create($title, $description, $attachment, $preview, $course);
        $this->lessonRepository->save($lesson);

        $this->eventDispatcher->dispatchAll($lesson->releaseEvents());

        return $lesson;
    }

    /**
     * @param int $id
     * @param string $title
     * @param string $description
     * @param string $preview
     * @return Lesson
     */
    public function edit(int $id, string $title, string $description, string $preview): Lesson
    {
        /** @var Lesson $lesson */
        $lesson = $this->lessonRepository->findById($id);
        $lesson->edit($title, $description, $preview);
        $this->lessonRepository->update($lesson);

        $this->eventDispatcher->dispatchAll($lesson->releaseEvents());

        return $lesson;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function remove(int $id): bool
    {
        $lesson = $this->lessonRepository->findById($id);
        $status = $this->lessonRepository->remove($lesson);

        $this->eventDispatcher->dispatchAll($lesson->releaseEvents());

        return $status;
    }
}
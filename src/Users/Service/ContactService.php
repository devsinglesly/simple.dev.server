<?php

namespace SimpleDev\Users\Service;


use SimpleDev\Users\Model\Contact;
use SimpleDev\Users\Repository\ContactRepositoryInterface;

class ContactService
{

    private $repository;

    public function __construct(ContactRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function createEmpty(): Contact
    {
        return $this->repository->createEmpty();
    }
}
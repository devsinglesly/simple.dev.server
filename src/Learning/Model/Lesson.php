<?php

namespace SimpleDev\Learning\Model;

use Doctrine\ORM\Mapping as ORM;
use SimpleDev\Users\Model\User;

/**
 * Class Lesson
 * @package SimpleDev\Learning\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Learning\Repository\LessonRepository")
 * @ORM\Table(name="learning_lessons")
 */
class Lesson
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="SimpleDev\Users\Model\User")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $teacher;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="SimpleDev\Learning\Model\Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=false)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $preview;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $repository;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $youtube;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isPublished = false;


    public static function create(
        string $title, string $description, string $youtube, User $teacher, Category $category,
        string $preview = null, string $repository = null): self
    {

        $lesson = (new self)
                ->changeTitle($title)
                ->changeYoutube($youtube)
                ->changeDescription($description)
                ->changePreview($preview)
                ->changeRepository($repository)
                ->changeTeacher($teacher)
                ->changeCategory($category);

        return $lesson;
    }

    public function changeTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function changeDescription(string $description):self
    {
        $this->description = $description;

        return $this;
    }

    public function changeYoutube(string $youtube):self
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function changePreview(?string $preview): self
    {
        $this->preview = $preview;

        return $this;
    }

    public function changeRepository(?string $repository):self
    {
        $this->repository = $repository;

        return $this;
    }

    public function changePublished(): self
    {
        $this->isPublished = !$this->isPublished;

        return $this;
    }

    public function changeCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }


    public function changeTeacher(User $teacher): self
    {
        $this->teacher = $teacher;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getTeacher(): User
    {
        return $this->teacher;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getPreview(): ?string
    {
        return $this->preview;
    }

    /**
     * @return string|null
     */
    public function getRepository(): ?string
    {
        return $this->repository;
    }

    /**
     * @return boolean
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    /**
     * @return string
     */
    public function getYoutube(): string
    {
        return $this->youtube;
    }

}
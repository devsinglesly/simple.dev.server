<?php
declare(strict_types=1);

namespace App\EventListener\Users;


use SimpleDev\Users\Event\UserSendPasswordEvent;

class UserSendPasswordListener
{

    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    public function handle(UserSendPasswordEvent $event): void
    {
        $message = new \Swift_Message('Регистрация');
        $message->setFrom('no-reply@simple-development.ru');
        $message->setTo($event->user->getEmail());

        ob_start();
        require_once __DIR__ . './../../../templates/mail/user.send.password.php';
        $content = ob_get_clean();

        $message->setBody($content, "text/html");

        $this->mailer->send($message);
    }
}
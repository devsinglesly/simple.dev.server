<?php

namespace SimpleDev\Users\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use SimpleDev\Users\Model\Repository;

class RepositoryRepository extends ServiceEntityRepository implements RepositoryRepositoryInterface
{
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $objectManager)
    {
        parent::__construct($registry, Repository::class);
        $this->manager = $objectManager;
    }
}
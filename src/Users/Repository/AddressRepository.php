<?php

namespace SimpleDev\Users\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use SimpleDev\Users\Model\Address;

class AddressRepository extends ServiceEntityRepository implements AddressRepositoryInterface
{

    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Address::class);
        $this->manager = $manager;
    }


    public function save(Address $address): Address
    {
        $this->manager->persist($address);
        $this->manager->flush();
        return $address;
    }
}
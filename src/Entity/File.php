<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File as CoreFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FileRepository")
 */
class File
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hash;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mimeType;

    public function __construct(CoreFile $file)
    {
        $this->hash = md5(uniqid());
        $this->path = $file->getPath();
        $this->fileName = $file->getFilename();
        $this->mimeType = $file->getMimeType();
    }

    public function getId(): ?int { return $this->id; }

    public function getHash(): ?string { return $this->hash; }

    public function getPath(): ?string { return $this->path; }

    public function getFileName(): ?string { return $this->fileName; }

    public function getFullPath(): ?string {
        return $this->getPath() . "/" . $this->getFileName();
    }

    /**
     * @return mixed
     */
    public function getMimeType(): ?string { return $this->mimeType; }

}

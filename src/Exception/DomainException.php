<?php
/**
 * Created by PhpStorm.
 * Users: devsinglesly
 * Date: 2/9/19
 * Time: 2:25 PM
 */

namespace App\Exception;


use Throwable;

class DomainException extends \DomainException implements OutableExceptionInterface
{
    public function __construct(string $message = "", int $code = 403, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
<?php

namespace SimpleDev\Users\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use phpDocumentor\Reflection\Types\Parent_;
use SimpleDev\Users\Model\User;

/**
 * Class UserRepository
 * @package SimpleDev\Users\Repository
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface
{
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, User::class);
        $this->manager = $manager;
    }

    /**
     * @param array $criteria
     * @return User
     */
    public function findOneByFields(array $criteria): User
    {
        /** @var User $user */
        $user = $this->findOneBy($criteria);
        if($user == null)
        {
            throw new NotFoundException("Пользователь не найден");
        }
        return $user;
    }

    public function save(User $user) : User
    {
        $this->manager->persist($user);
        $this->manager->flush();

        return $user;
    }

    public function update(User $user): User
    {
        $this->manager->flush();
        return $user;
    }

    /**
     * @param string $email
     * @return User
     */
    public function findOneByEmail(string $email): User
    {
        return $this->findOneByFields(['email' => $email]);
    }

    /**
     * @param array $criteria
     * @return User|null
     */
    public function findOneByOrNull(array $criteria): ?User
    {
        return $this->findOneBy($criteria);
    }

    /**
     * @param array $criteria
     * @param array $order
     * @param int $offset
     * @param int $limit
     * @return User[]
     */
    public function findAllBy(array $criteria, array $order, int $offset, int $limit): array
    {
        return parent::findBy($criteria, $order, $limit, $offset);
    }
}
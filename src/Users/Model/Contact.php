<?php

namespace SimpleDev\Users\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class Contact
 * @package SimpleDev\Users\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Users\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $vk;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $skype;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $discord;


    /**
     * @return string
     */
    public function getVk(): ?string
    {
        return $this->vk;
    }

    /**
     * @return string
     */
    public function getSkype(): ?string
    {
        return $this->skype;
    }

    /**
     * @return string
     */
    public function getDiscord(): ?string
    {
        return $this->discord;
    }


}
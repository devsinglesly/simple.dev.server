<?php

namespace SimpleDev\Users\Repository;


use SimpleDev\Users\Model\Contact;

interface ContactRepositoryInterface
{
    public function createEmpty(): Contact;
}
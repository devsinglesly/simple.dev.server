<?php

namespace SimpleDev\Users\Event;


use SimpleDev\Users\Model\User;
use Symfony\Component\EventDispatcher\Event;

class CreateFromBidEvent extends Event
{
    public $user, $price;

    public function __construct(User $user, int $price)
    {
        $this->user = $user;
        $this->price = $price;
    }
}
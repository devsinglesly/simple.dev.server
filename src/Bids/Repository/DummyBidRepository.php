<?php
declare(strict_types=1);

namespace SimpleDev\Bids\Repository;


use App\Dto\Email;
use App\Dto\Name;
use App\Dto\Phone;
use App\Exception\NotFoundException;
use ReflectionObject;
use SimpleDev\Bids\Model\Bid;

class DummyBidRepository implements BidRepositoryInterface
{
    private $id = 0;

    private function generateId(): int
    {
        $this->id++;
        return $this->id;
    }

    /**
     * @param Bid $bid
     * @return Bid
     */
    public function save(Bid $bid): Bid
    {
        $refl = new ReflectionObject($bid);
        $prop = $refl->getProperty('id');
        $prop->setAccessible(true);
        $prop->setValue($bid, $this->generateId());

        return $bid;
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param $offset
     * @return Bid[]
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array
    {
        return [
            $this->save(Bid::create(
                new Name("Last", "First"),
                18,
                new Email("some@gmail.com"),
                new Phone("+7 (999) 458 99-99"),
                "status 1",
                "about"
            )),
            $this->save(Bid::create(
                new Name("Last", "First"),
                18,
                new Email("some@gmail.com"),
                new Phone("+7 (999) 458 99-99"),
                "status 1",
                "about"
            )),
            $this->save(Bid::create(
                new Name("Last", "First"),
                18,
                new Email("some@gmail.com"),
                new Phone("+7 (999) 458 99-99"),
                "status 1",
                "about"
            ))
        ];
    }

    /**
     * @param int $id
     * @return Bid
     * @throws NotFoundException
     * @throws \Exception
     */
    public function one(int $id): Bid
    {
        return Bid::create(
            new Name("Last", "First"),
            18,
            new Email("some@gmail.com"),
            new Phone("+7 (999) 458 99-99"),
            "status 1",
            "about"
        );
    }
}
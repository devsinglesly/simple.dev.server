<?php
declare(strict_types=1);

namespace App\Tests\Dto;


use App\Dto\Email;
use App\Exception\RuntimeException;
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{

    final public function emailProvider(): array
    {
        return [
            ['some@gmail.com'],
            ['test123@gmail.com'],
            ['inde0x22@yandex.ru'],
            ['222c-orrect23123@l.ru'],
            ['yda-xa@m.r'],
            ['yaw23d1da.dwa@mail.ru']
        ];
    }

    /**
     * @dataProvider emailProvider
     * @param string $data
     */
    final public function testCorrect(string $data): void
    {
        $email = new Email($data);

        $this->assertEquals($email->get(), $data);

    }


    final public function emailIncorrects(): array
    {
        return [
            ["123"],
            ["2131@"],
            ["111@.rr"]
        ];
    }

    /**
     * @dataProvider  emailIncorrects
     * @param string $data
     */
    final public function testRuntimeException(string $data): void
    {
        $this->expectException(RuntimeException::class);

        $email = new Email($data);
    }





}
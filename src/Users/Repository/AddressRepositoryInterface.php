<?php

namespace SimpleDev\Users\Repository;


use SimpleDev\Users\Model\Address;

interface AddressRepositoryInterface
{
    public function save(Address $address): Address;
}
<?php

namespace SimpleDev\Courses\Model;


use SimpleDev\Courses\Event\EventTrait;
use SimpleDev\Courses\Event\Lesson\LessonCreateEvent;
use SimpleDev\Courses\Event\Lesson\LessonEditEvent;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SimpleDev\Courses\Repository\LessonRepository")
 * @ORM\Table(name="courses_lesson")
 */
class Lesson
{
    use EventTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $attachment;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $preview;

    /**
     * @ORM\ManyToOne(targetEntity="SimpleDev\Courses\Model\Course")
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Course
     */
    private $course;

    private function __construct(){}

    /**
     * @param string $title
     * @param string $description
     * @param string $attachment
     * @param string $preview
     * @param Course $course
     * @return Lesson
     */
    public static function create(string $title, string $description, string $attachment, string $preview, Course $course): self
    {
        $lesson = new self();
        $lesson->title = $title;
        $lesson->description = $description;
        $lesson->attachment = $attachment;
        $lesson->preview = $preview;
        $lesson->course = $course;

        $lesson->recordEvent(new LessonCreateEvent($lesson));

        return $lesson;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $preview
     * @return Lesson
     */
    public function edit(string $title, string $description, string $preview): self
    {
        $this->title = $title;
        $this->description = $description;
        $this->preview = $preview;

        $this->recordEvent(new LessonEditEvent($this));

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getAttachment(): string
    {
        return $this->attachment;
    }

    /**
     * @return string
     */
    public function getPreview(): string
    {
        return $this->preview;
    }

    /**
     * @return Course
     */
    public function getCourse(): Course
    {
        return $this->course;
    }

}
<?php

namespace App\Controller\Courses;

use SimpleDev\Courses\DTO\Price;
use SimpleDev\Courses\Model\Course;
use SimpleDev\Courses\Service\CourseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class CourseController extends AbstractController
{
    /**
     * @var CourseService
     */
    private $courseService;

    public function __construct(CourseService $courseService)
    {
        $this->courseService = $courseService;
    }

    /**
     * @Route("", methods={"GET"})
     * @return JsonResponse
     */
    public function getPublished(): JsonResponse
    {
        /** @var Course[] $courses */
        $courses = $this->courseService->getPublished();

        return $this->json($courses);
    }

    /**
     * @Route("/getAll", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request) : JsonResponse
    {
        $courses = $this->courseService->getAll();

        return $this->json($courses);
    }

    /**
     * @Route("/{id}/one", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function getById(int $id): JsonResponse
    {
        $course = $this->courseService->getById($id);

        return $this->json($course);
    }

    /**
     * @Route("/create", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function new(Request $request) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);


        $course = $this->courseService->create(
            (string)$data['title'],
            (string)$data['description'],
            (string)$data['preview'],
            new Price((int)$data['price'])
        );

        return $this->json($course);
    }

    /**
     * @Route("/publish/{id}", methods={"PUT"})
     * @param int $id
     * @return JsonResponse
     */
    public function publish(int $id): JsonResponse
    {
        $course = $this->courseService->publish((int)$id);

        return $this->json($course);
    }

    /**
     * @Route("/hide/{id}", methods={"PUT"})
     * @param int $id
     * @return JsonResponse
     */
    public function hide(int $id): JsonResponse
    {
        $course = $this->courseService->hide($id);

        return $this->json($course);
    }

    /**
     * @Route("/edit/{id}", methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $course = $this->courseService->edit(
            $id,
            $data['title'],
            $data['description'],
            $data['preview']
        );

        return $this->json($course);
    }

    /**
     * @Route("/price/{id}", methods={"PUT"})
     * @param Request $request
     * @return JsonResponse
     */
    public function changePrice(Request $request, int $id) : JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $course = $this->courseService->changePrice($id, new Price((int)$data['price']));

        return $this->json($course);
    }

    /**
     * @Route("/remove/{id}", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function remove(int $id) : JsonResponse
    {
        $this->courseService->remove($id);

        return $this->json(['status' => true]);
    }
}

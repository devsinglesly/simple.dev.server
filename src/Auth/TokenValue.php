<?php

namespace App\Auth;



class TokenValue
{

    private $value = "";

    /**
     * TokenValue constructor.
     * @param string $value
     * @throws JsonWebTokenException
     */
    public function __construct(string $value)
    {
        $this->value = $this->validate($value);
    }

    public function __toString()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     * @throws JsonWebTokenException
     */
    protected function validate(string $value): string
    {
        $pattern = "#(.+)\.(.+)\.(.+)#";

        if(!preg_match($pattern, $value))
        {
            throw new JsonWebTokenException("Incorrect token string format");
        }

        return $value;
    }
}
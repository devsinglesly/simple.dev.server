<?php
declare(strict_types=1);

namespace App\EventListener;


use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class CorsListener
{
    public function onKernelResponse(FilterResponseEvent $event): FilterResponseEvent
    {
        $headers = $event->getResponse()->headers;

        $headers->set('Access-Control-Allow-Headers', 'Content-Type, HTTP_Authorization');

        return $event;
    }
}
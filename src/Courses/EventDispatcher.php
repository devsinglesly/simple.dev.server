<?php

namespace SimpleDev\Courses;

use SimpleDev\Courses\Event\Event;
use SimpleDev\Courses\Event\EventDispatcherInterface;


class EventDispatcher implements EventDispatcherInterface
{

    private $listeners = [];

    public function __construct(array $listeners = [])
    {
        $this->listeners = $listeners;
    }

    public function addEventListener(string $event, $listener) : self
    {
        if(!class_exists($listener))
        {
            throw new \RuntimeException("Class listener is not exists");
        }

        $this->listeners[$event][] = $listener;

        return $this;
    }

    public function dispatch(Event $event) : void
    {
        if(!isset($this->listeners[get_class($event)]))
        {
            return;
        }
        /** @var string $listener */
        foreach ($this->listeners[get_class($event)] as $listener) {

            $eventListener = new $listener;
            call_user_func([$eventListener, 'handle'], $event);

        }
    }

    /**
     * @param Event[] $events
     */
    public function dispatchAll(array $events): void
    {
        foreach ($events as $event)
        {
            $this->dispatch($event);
        }
    }
}
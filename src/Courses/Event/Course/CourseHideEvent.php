<?php

namespace SimpleDev\Courses\Event\Course;


use SimpleDev\Courses\Event\Event;
use SimpleDev\Courses\Model\Course;

class CourseHideEvent extends Event
{
    public $course;

    public function __construct(Course $course)
    {
        $this->course = $course;
    }
}
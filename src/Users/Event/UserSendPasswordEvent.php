<?php
declare(strict_types=1);

namespace SimpleDev\Users\Event;


use SimpleDev\Users\Model\User;
use Symfony\Component\EventDispatcher\Event;

class UserSendPasswordEvent extends Event
{
    public $password;
    public $user;

    public function __construct(User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
}
<?php

namespace SimpleDev\Courses\Model;


use LogicException;
use SimpleDev\Courses\DTO\Price;
use SimpleDev\Courses\Event\Course\CourseChangePriceEvent;
use SimpleDev\Courses\Event\Course\CourseCreateEvent;
use SimpleDev\Courses\Event\Course\CourseEditEvent;
use SimpleDev\Courses\Event\Course\CourseHideEvent;
use SimpleDev\Courses\Event\Course\CoursePublishEvent;
use SimpleDev\Courses\Event\EventTrait;
use SimpleDev\Courses\Model\Lesson;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="SimpleDev\Courses\Repository\CourseRepository")
 * @ORM\Table(name="courses_course")
 */
class Course
{
    use EventTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @var string
     */
    private $description;

    /**
     * @ORM\Column(type="integer", length=11)
     * @var integer
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $preview;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $publish = false;


    public function __construct(){}

    /**
     * @param string $title
     * @param string $description
     * @param Price $price
     * @return Course
     */
    public static function create(string $title, string $description, string $preview, Price $price) : self
    {
        $course = new self();
        $course->title =  $title;
        $course->description = $description;
        $course->preview = $preview;
        $course->price = $price->getCost();

        $course->recordEvent(new CourseCreateEvent($course));

        return $course;
    }

    /**
     * @param string $title
     * @param string $description
     * @return Course
     */
    public function edit(string $title, string $description, string $preview) : self
    {
        $this->title = $title;
        $this->description = $description;
        $this->preview = $preview;

        $this->recordEvent(new CourseEditEvent($this));

        return $this;
    }

    /**
     * @param Price $price
     * @return Course
     */
    public function changePrice(Price $price) : self
    {
        $this->price = $price->getCost();

        $this->recordEvent(new CourseChangePriceEvent($this));

        return $this;
    }

    /**
     * @return Course
     */
    public function publish() : self
    {
        if($this->isPublished())
        {
            throw new LogicException("Курс уже опубликован !");
        }
        $this->publish = true;

        $this->recordEvent(new CoursePublishEvent($this));

        return $this;
    }

    /**
     * @return Course
     */
    public function hide() : self
    {
        if(!$this->isPublished())
        {
            throw new LogicException("Курс и так не опубликован");
        }
        $this->publish = false;

        $this->recordEvent(new CourseHideEvent($this));

        return $this;
    }

    /**
     * @return bool
     */
    public function isPublished() : bool
    {
        return $this->publish;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPreview(): string
    {
        return $this->preview;
    }
}
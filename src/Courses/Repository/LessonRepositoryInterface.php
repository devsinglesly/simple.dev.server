<?php

namespace SimpleDev\Courses\Repository;

use SimpleDev\Courses\Model\Lesson;

interface LessonRepositoryInterface
{
    /**
     * @param array $criteria
     * @return Lesson[]
     */
    public function findByParams(array $criteria): array;

    /**
     * @return Lesson[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Lesson
     */
    public function findById(int $id): Lesson;

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function save(Lesson $lesson): Lesson;

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function update(Lesson $lesson): Lesson;

    /**
     * @param Lesson $lesson
     * @return bool
     */
    public function remove(Lesson $lesson): bool;

}
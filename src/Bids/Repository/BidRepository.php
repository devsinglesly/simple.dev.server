<?php

namespace SimpleDev\Bids\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use SimpleDev\Bids\Model\Bid;

/**
 * Class BidRepository
 * @package SimpleDev\Bid\Repository
 * @method findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BidRepository extends ServiceEntityRepository implements BidRepositoryInterface
{

    private $manager;


    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Bid::class);
        $this->manager = $manager;
    }


    /**
     * @param Bid $bid
     * @return Bid
     */
    public function save(Bid $bid): Bid
    {
        $this->manager->persist($bid);
        $this->manager->flush();
        return $bid;
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param $offset
     * @return Bid[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array
    {
        /** @var Bid[] $items */
        $items = parent::findBy($criteria, $orderBy, $limit, $offset);
        return $items;
    }

    /**
     * @inheritdoc
     */
    public function one(int $id): Bid
    {

        /** @var Bid $bid */
        $bid = parent::findOneBy(['id' => $id]);

        if($bid == null){
            throw new NotFoundException("Форма не найдена");
        }

        return $bid;
    }
}
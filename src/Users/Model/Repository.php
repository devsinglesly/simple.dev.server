<?php

namespace SimpleDev\Users\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Repository
 * @package SimpleDev\Users\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Users\Repository\RepositoryRepository")
 */
class Repository
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $link;

    /**
     * @return string
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
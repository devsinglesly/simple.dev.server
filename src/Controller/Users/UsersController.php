<?php

namespace App\Controller\Users;

use App\Auth\TokenData;
use App\Exception\AuthException;
use SimpleDev\Bids\Service\BidService;
use SimpleDev\Users\Model\User;
use SimpleDev\Users\Service\AuthService;
use SimpleDev\Users\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class UsersController extends AbstractController
{
    /**
     * @var UserService
     */
    private $usersService;

    private $bidService;

    public function __construct(UserService $usersService, BidService $bidService)
    {
        $this->usersService = $usersService;
        $this->bidService = $bidService;
    }

    /**
     * @Route("/", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $criteria = [];
        $order = "DESC";
        $limit = 10;
        $offset = 0;

        if($request->query->has('type'))
            $criteria['type'] = (int)$request->query->get('type');

        if($request->query->has('active'))
            $criteria['isActive'] = (boolean)$request->query->get('active');

        if($request->query->has('order'))
        {
            $orderQuery = $request->query->get('order');

            if(preg_match("#^asc$#i", $orderQuery))
                $order = "ASC";

            if(preg_match("#^desc$#i", $orderQuery))
                $order = "DESC";
        }

        if($request->query->has('limit'))
            $limit = (int)$request->query->get('limit');

        if($request->query->has('offset'))
            $offset = (int)$request->query->get('offset');

        /** @var User[] $users */
        $users = $this->usersService->getAll($criteria, ['id' => $order], $offset, $limit);

        return $this->json($users);
    }

    /**
     * @Route("/bid/create/{id}", methods={"POST"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function signupFromBid(Request $request, int $id): JsonResponse
    {

        $data = json_decode($request->getContent());

        $bid = $this->bidService->getOne($id);

        $this->usersService->createFromBid($bid, $data->price);

        return $this->json(true);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/send/password", methods={"POST"})
     */
    public function sendPassword(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent());

        $result = $this->usersService->sendPassword($data->uid);

        return $this->json($result);

    }

}

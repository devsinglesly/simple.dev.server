<?php
declare(strict_types=1);

namespace App\Tests\Dto;


use App\Dto\Phone;
use App\Exception\RuntimeException;
use PHPUnit\Framework\TestCase;

final class PhoneTest extends TestCase
{
    /**
     * @return array
     */
    final public function correctPhonesProvider() : array
    {
        return [
            ["+1 (111) 222 22-88"],
            ["+2 (123) 999 89-88"],
            ["+3 (929) 999 89-88"],
            ["+4 (939) 999 89-88"],
            ["+5 (566) 999 89-88"],
            ["+6 (999) 999 89-88"],
            ["+7 (999) 999 89-88"]
        ];
    }

    /**
     * @param string $phone
     * @dataProvider correctPhonesProvider
     */
    final public function testCorrectPhones(string $phone) : void
    {
        $item = new Phone($phone);

        $this->assertEquals($phone, (string)$item);
        $this->assertEquals($phone, $item->get());
        $this->assertIsString($phone, $item->get());
    }

    /**
     * @return array
     */
    final public function incorrectPhonesProvider(): array
    {
        return [
            ["7 777 777 77 77"],
            ["+7 777 777 77 77"],
            ["+7 (777) 777 77 77"],
            ["+7 (фцв) 777 77-77"],
            ["+7 (wad) 777 77-77"],
            [""]
        ];
    }

    /**
     * @param string $phone
     * @dataProvider incorrectPhonesProvider
     */
    final public function testIncorrectPhones(string $phone): void
    {
        $this->expectException(RuntimeException::class);

        $item = new Phone($phone);
    }
}
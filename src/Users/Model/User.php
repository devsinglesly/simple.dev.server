<?php

namespace SimpleDev\Users\Model;

use App\Exception\DomainException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SimpleDev\Users\DTO\Name;
use SimpleDev\Users\ValueObject\Email;
use SimpleDev\Users\ValueObject\Password;

/**
 * Class User
 * @package SimpleDev\Users\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Users\Repository\UserRepository")
 */
class User
{
    const ACTIVE = 1;
    const NON_ACTIVE = 0;

    const TYPE_DEFAULT = 0;
    const TYPE_STUDENT = 1;
    const TYPE_MENTOR = 2;
    const TYPE_ADMIN = 3;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue();
     * @ORM\Column(type="integer", length=11)
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $middleName;

    /**
     * @ORM\Column(type="integer", length=3, nullable=true)
     * @var integer
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $password;

    /**
     * @ORM\Column(type="smallint")
     * @var int
     */
    private $type;

    /**
     * @ORM\OneToOne(targetEntity="Address", cascade={"persist"})
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     * @var Address
     */
    private $address;

    /**
     * @ORM\OneToOne(targetEntity="Contact", cascade={"persist"})
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * @var Contact
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity="Phone", mappedBy="user", cascade={"all"})
     * @var Collection
     */
    private $phones;

    /**
     * @ORM\OneToMany(targetEntity="Repository", mappedBy="user", cascade={"persist"})
     * @var Collection
     */
    private $repositories;

    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $isActive;


    public function __construct()
    {
        $this->phones = new ArrayCollection();
        $this->repositories = new ArrayCollection();
    }

    public static function signup(Email $email, Password $password): self
    {
        $user = new self();
        $user->address = new Address();
        $user->contact = new Contact();
        $user->email = (string)$email;
        $user->setPassword($password->getValue());
        $user->isActive = self::ACTIVE;
        $user->type = self::TYPE_DEFAULT;

        return $user;
    }

    public static function create(Email $email) : self
    {
        $user = new self();
        $user->email = (string)$email;
        $user->address = new Address();
        $user->contact = new Contact();
        $user->isActive = self::NON_ACTIVE;
        $user->type = self::TYPE_DEFAULT;

        return $user;
    }


    public function setPassword(string $password): self
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);

        return $this;
    }

    public function activate(): self
    {
        $this->isActive = self::ACTIVE;

        return $this;
    }


    public function changeAddress(Address $address) : self
    {
        $this->address = $address;

        return $this;
    }

    public function changeContact(Contact $contact) : self
    {
        $this->contact = $contact;

        return $this;
    }


    public function passwordVerify(string $password): bool
    {
        if(!password_verify($password, $this->password))
        {
            throw new DomainException("Неверный пароль");
        }
        return true;
    }

    public function changeAge(int $age): self
    {
        if($age <= 0){
            throw new \DomainException("Некорректный возраст");
        }
        $this->age = $age;

        return $this;
    }

    public function changeName(Name $name): self
    {
        $this->lastName = $name->getLast();
        $this->middleName = $name->getMiddle();
        $this->firstName = $name->getFirst();

        return $this;
    }

    public function disableActive(): self
    {
        $this->isActive = self::NON_ACTIVE;

        return $this;
    }

    /**
     * @param Phone $phone
     * @return User
     */
    public function addPhone(Phone $phone): self
    {
        $phone->set($this);

        $this->phones->add($phone);

        return $this;
    }


    /**
     * @return int
     */
    public function getId(): int{return $this->id;}

    /**
     * @return string
     */
    public function getLastName(): ?string{return $this->lastName;}

    /**
     * @return string
     */
    public function getFirstName(): ?string{return $this->firstName;}

    /**
     * @return string
     */
    public function getMiddleName(): ?string{return $this->middleName;}

    /**
     * @return int
     */
    public function getAge(): ?int{return $this->age;}

    /**
     * @return string
     */
    public function getEmail(): string{return $this->email;}

    /**
     * @return Contact
     */
    public function getContact() : Contact{return $this->contact;}

    /**
     * @return int
     */
    public function getType(): int{return $this->type;}

    /**
     * @return Address
     */
    public function getAddress(): Address{return $this->address;}

    public function isDefault(): bool
    {
        return $this->type === self::TYPE_DEFAULT;
    }

    public function isStudent(): bool
    {
        return $this->type === self::TYPE_STUDENT;
    }

    public function isAdmin(): bool
    {
        return $this->type === self::TYPE_ADMIN;
    }

    public function isMentor(): bool
    {
        return $this->type === self::TYPE_MENTOR;
    }

    public function isActive(): bool
    {
        return $this->isActive === self::ACTIVE;
    }

    public function guardIsAdminOrMentor(): void
    {
        if(!$this->isAdmin() || !$this->isMentor()){
            throw new DomainException("Недостаточно прав");
        }
    }


    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => [
                'last' => $this->getLastName(),
                'first' => $this->getFirstName(),
                'middle' => $this->getMiddleName()
            ],
            'email' => $this->getEmail(),
            'type' => $this->getType(),
            'age' => $this->getAge(),
            'contact' => $this->getContact(),
            'address' => $this->getAddress()
        ];
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->phones->toArray();
    }
}
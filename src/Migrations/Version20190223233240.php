<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190223233240 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lesson DROP FOREIGN KEY FK_F87474F312469DE2');
        $this->addSql('CREATE TABLE learning_categories (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, preview VARCHAR(255) DEFAULT NULL, is_published TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE learning_lessons (id INT AUTO_INCREMENT NOT NULL, teacher_id INT DEFAULT NULL, category_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, preview LONGTEXT DEFAULT NULL, repository VARCHAR(255) DEFAULT NULL, youtube VARCHAR(255) NOT NULL, is_published TINYINT(1) NOT NULL, INDEX IDX_FBE5A02A41807E1D (teacher_id), INDEX IDX_FBE5A02A12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE learning_lessons ADD CONSTRAINT FK_FBE5A02A41807E1D FOREIGN KEY (teacher_id) REFERENCES user (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE learning_lessons ADD CONSTRAINT FK_FBE5A02A12469DE2 FOREIGN KEY (category_id) REFERENCES learning_categories (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE lesson');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE learning_lessons DROP FOREIGN KEY FK_FBE5A02A12469DE2');
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, preview VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, is_published TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE lesson (id INT AUTO_INCREMENT NOT NULL, teacher_id INT DEFAULT NULL, category_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, description LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, preview LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci, repository VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, youtube VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, is_published TINYINT(1) NOT NULL, INDEX IDX_F87474F341807E1D (teacher_id), INDEX IDX_F87474F312469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE lesson ADD CONSTRAINT FK_F87474F341807E1D FOREIGN KEY (teacher_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('DROP TABLE learning_categories');
        $this->addSql('DROP TABLE learning_lessons');
    }
}

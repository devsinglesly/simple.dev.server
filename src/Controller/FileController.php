<?php

namespace App\Controller;

use App\Entity\File;
use App\Exception\NotFoundException;
use App\Service\FileService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File as CoreFile;

/**
 * Class FileController
 * @package App\Controller
 * @Route("/file")
 */
class FileController extends AbstractController
{

    private $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @Route("/{hash}", methods={"GET"})
     * @param string $hash
     * @return Response
     * @throws NotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
	public function index(string $hash)
	{
	    $file = $this->fileService->getResource($hash);

		return $this->file($file, md5(uniqid()), ResponseHeaderBag::DISPOSITION_INLINE);
	}

    /**
     * @Route("/{hash}/download", methods={"GET"})
     * @param string $hash
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws NotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function download(string $hash)
    {
        $file = $this->fileService->getResource($hash);

        return $this->file(
            $file,
            md5(uniqid()),
            ResponseHeaderBag::DISPOSITION_ATTACHMENT
        );
    }

    /**
     * @Route("/upload", name="file_upload", methods={"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function upload(Request $request)
    {
		$file = $request->files->get('file');

		$entityFile = $this->fileService->save($file);

		return $this->json([
		    'hash' => $entityFile->getHash()
        ]);
    }
}

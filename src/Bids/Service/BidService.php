<?php

namespace SimpleDev\Bids\Service;


use App\Dto\Email;
use App\Dto\Name;
use App\Dto\Phone;
use SimpleDev\Bids\Repository\BidRepositoryInterface;
use SimpleDev\Bids\Model\Bid;

class BidService
{
    private $repository;

    public function __construct(BidRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Name $name
     * @param int $age
     * @param Phone $phone
     * @param Email $email
     * @param string $status
     * @param string $info
     * @return Bid
     * @throws \Exception
     */
    public function create(Name $name, int $age, Phone $phone, Email $email, string $status, string $info): Bid
    {
        $bid = Bid::create($name, $age, $email, $phone, $status, $info);
        $this->repository->save($bid);
        return $bid;
    }

    public function getAll(array $criteria, array $orderBy, int $offset, int $limit): array
    {
        return $this->repository->getAll($criteria, $orderBy, $limit, $offset);
    }

    public function getOne(int $id): Bid
    {
        return $this->repository->one($id);
    }

}
<?php

namespace App\Controller\Courses;

use SimpleDev\Courses\Model\Lesson;
use SimpleDev\Courses\Service\CourseService;
use SimpleDev\Courses\Service\LessonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LessonController
 * @package App\Controller\Courses
 * @Route("/lesson")
 */
class LessonController extends AbstractController
{

    private $lessonService, $courseService;

    public function __construct(LessonService $lessonService, CourseService $courseService)
    {
        $this->lessonService = $lessonService;
        $this->courseService = $courseService;
    }

    /**
     * @Route("/list/{id}")
     * @param int $id
     * @return JsonResponse
     */
    public function getAllByCourseId(int $id): JsonResponse
    {
        /** @var Lesson[] $lessons */
        $lessons = $this->lessonService->getAllByCourseId($id);

        return $this->json($lessons);
    }

    /**
     * @Route("/list")
     * @return JsonResponse
     */
    public function getAll(): JsonResponse
    {
        $lessons = $this->lessonService->getAll();

        return $this->json($lessons);
    }

    /**
     * @Route("/create")
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $course = $this->courseService->getById((int)$data['course_id']);

        $lesson = $this->lessonService->create(
            $data['title'],
            $data['description'],
            $data['attachment'],
            $data['preview'],
            $course
        );

        return $this->json($lesson);
    }


    /**
     * @Route("/edit/{id}")
     * @param Request $request
     * @return JsonResponse
     */
    public function edit(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $lesson = $this->lessonService->edit($id, $data['title'], $data['description'], $data['preview']);

        return $this->json($lesson);
    }


    /**
     * @Route("/remove/{id}")
     * @param int $id
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {

        $this->lessonService->remove($id);

        return $this->json(['status' => true]);
    }

}

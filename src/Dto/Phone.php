<?php

namespace App\Dto;


use App\Exception\RuntimeException;

class Phone
{

    private $value;

    /**
     * Phone constructor.
     * @param string $value
     * @throws RuntimeException
     */
    public function __construct(string $value)
    {
        $pattern = "\+\d{1} \(\d{3}\) \d{3} \d{2}\-\d{2}";

        if(!preg_match("#^{$pattern}$#i", $value))
            throw new RuntimeException("Не корректный телефон");

        $this->value = $value;
    }

    public function get(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->get();
    }
}
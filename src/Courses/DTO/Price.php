<?php

namespace SimpleDev\Courses\DTO;


use LogicException;

class Price
{
    /** @var integer */
    private $cost;

    public function __construct(int $cost)
    {
        if($cost <= 0)
        {
            throw new LogicException("Стоимость не может быть меньше 0");
        }

        $this->cost = $cost;
    }

    public function getCost() : int
    {
        return $this->cost;
    }



}
<?php

namespace SimpleDev\Bids\Repository;

use App\Exception\NotFoundException;
use SimpleDev\Bids\Model\Bid;

interface BidRepositoryInterface
{
    /**
     * @param Bid $bid
     * @return Bid
     */
    public function save(Bid $bid): Bid;

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param $offset
     * @return Bid[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array;

    /**
     * @param int $id
     * @return Bid
     * @throws NotFoundException
     */
    public function one(int $id): Bid;
}
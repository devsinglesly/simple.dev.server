<?php
declare(strict_types=1);

namespace App\Tests\Controller\Bids;


use App\Controller\Bids\BidController;
use PHPUnit\Framework\TestCase;
use SimpleDev\Bids\Repository\DummyBidRepository;
use SimpleDev\Bids\Service\BidService;
use Symfony\Component\HttpFoundation\Request;

class BidControllerTest extends TestCase
{
    /**
     * @var BidController
     */
    public $controller;

    public function setup() : void
    {
       $bidService = new BidService(new DummyBidRepository());
       $this->controller = new BidController($bidService);
    }


    final public function createProvider()
    {
        return [
            [
                'lastname', 'firstname', 'email@email.com', '+7 (999) 999 99-99', 'info', 'status', 'age'

            ]
        ];
    }

    /**
     * @dataProvider createProvider
     * @param string $last
     * @param string $first
     * @param string $email
     * @param string $phone
     * @param string $info
     * @param string $status
     * @param string $age
     * @throws \ReflectionException
     * @throws \Exception
     */
    final public function testCreateSuccess(
        string $last, string $first, string $email, string $phone, string $info, string $status, string $age
    ): void
    {
        $request = $this
            ->getMockBuilder(Request::class)
            ->setMethods(['getContent'])
            ->getMock();

        $request
            ->expects($this->once())
            ->method('getContent')
            ->willReturn(json_encode([
                'last_name' => $last,
                'first_name' => $first,
                'info' => $info,
                'status' => $status,
                'age' => (int)$age,
                'email' => $email,
                'phone' => $phone
            ]));


        $response = $this->controller->create($request);

        $content = json_decode($response->getContent());

        $this->assertEquals($last, $content->last_name);
        $this->assertEquals($first, $content->first_name);


    }
}
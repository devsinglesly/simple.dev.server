<?php

namespace App\Controller\Learning;


use SimpleDev\Learning\Model\Lesson;
use SimpleDev\Learning\Service\CategoryService;
use SimpleDev\Learning\Service\LessonService;
use SimpleDev\Users\Service\AuthService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LessonController
 * @package App\Controller\Learning
 * @Route("/lesson")
 */
class LessonController extends AbstractController
{

    private $lessonService;

    private $authService;

    private $categoryService;

    public function __construct(LessonService $lessonService, AuthService $authService, CategoryService $categoryService)
    {
        $this->lessonService = $lessonService;
        $this->authService = $authService;
        $this->categoryService = $categoryService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/", methods={"GET"})
     */
    public function getAll(Request $request): JsonResponse
    {
        $order = $request->query->get('order') ?: 'DESC';
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;
        $category = $request->query->get('category') ?: null;

        $criteria = [];

        if($category != null){
            $criteria['category'] = (int)$category;
        }

        /** @var Lesson[] $lessons */
        $lessons = $this->lessonService->getAll($criteria, ['id' => $order], $limit, $offset);

        return $this->json($lessons);
    }

    /**
     * @Route("/{id}", methods={"GET"}, requirements={"id"="\d+"})
     * @param int $id
     * @return JsonResponse
     */
    public function getById(int $id): JsonResponse
    {
        return $this->json(
            $this->lessonService->one($id, ['isPublished' => true])
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/published", methods={"GET"})
     */
    public function getPublish(Request $request): JsonResponse
    {
        $order = $request->query->get('order') ?: 'DESC';
        $limit = $request->query->get('limit') ?: 10;
        $offset = $request->query->get('offset') ?: 0;
        $category = $request->query->get('category') ?: null;

        $criteria = [];

        if($category != null){
            $criteria['category'] = (int)$category;
        }

        /** @var Lesson[] $lessons */
        $lessons = $this->lessonService->getAll($criteria + ['isPublished'=> true], ['id' => $order], $limit, $offset);

        return $this->json($lessons);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \App\Auth\JsonWebTokenException
     * @Route("/", methods={"POST"})
     */
    public function new(Request $request): JsonResponse
    {

        $data = json_decode($request->getContent(), true);

        $teacher = $this->authService->getUserByToken($request->headers->get('Authorization'));

        $category = $this->categoryService->getOne((int)$data['category']);

        $lesson = $this->lessonService->create(
            $data['title'],
            $data['description'],
            $data['youtube'],
            $teacher,
            $category,
            $data['preview'],
            $data['repository']
        );

        return $this->json($lesson);
    }

    /**
     * @Route("/{id}/image", methods={"PATCH"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function image(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $lesson = $this->lessonService->image($id, $data['path']);

        return $this->json($lesson);
    }

    /**
     * @Route("/{id}", methods={"PUT"}, requirements={"id"="\d+"})
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $data = json_decode($request->getContent(), true);


        $category = $this->categoryService->getOne((int)$data['category']);

        $lesson = $this->lessonService->update(
            $id,
            $data['title'],
            $data['description'],
            $data['youtube'],
            $category,
            $data['preview'],
            $data['repository']
        );

        return $this->json($lesson);
    }

    /**
     * @param int $id
     * @return JsonResponse
     * @Route("/{id}/publish", methods={"PATCH"}, requirements={"id"="\d+"})
     */
    public function publish(int $id): JsonResponse
    {
        $lesson = $this->lessonService->publish($id);

        return $this->json($lesson);
    }


    /**
     * @Route("/{id}", methods={"DELETE"}, requirements={"id"="\d+"})
     * @param int $id
     * @return JsonResponse
     */
    public function remove(int $id): JsonResponse
    {
        $this->lessonService->remove($id);

        return $this->json(true);
    }
}
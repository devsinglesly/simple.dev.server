<?php
/**
 * Created by PhpStorm.
 * Users: devsinglesly
 * Date: 2/6/19
 * Time: 12:45 AM
 */

namespace App\Exception;


use Throwable;

class AuthException extends \DomainException implements OutableExceptionInterface
{
    public function __construct(string $message = "", int $code = 401, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
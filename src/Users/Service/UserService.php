<?php

namespace SimpleDev\Users\Service;


use App\Exception\DomainException;
use App\Exception\NotFoundException;
use SimpleDev\Bids\Model\Bid;
use SimpleDev\Bids\Repository\BidRepositoryInterface;
use SimpleDev\Users\DTO\Name;
use SimpleDev\Users\Event\CreateFromBidEvent;
use SimpleDev\Users\Event\UserSendPasswordEvent;
use SimpleDev\Users\Model\Phone;
use SimpleDev\Users\Model\User;
use SimpleDev\Users\Repository\AddressRepositoryInterface;
use SimpleDev\Users\Repository\ContactRepositoryInterface;
use SimpleDev\Users\Repository\UserRepositoryInterface;
use SimpleDev\Users\ValueObject\Email;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserService
{
    /** @var UserRepositoryInterface  */
    private $userRepository;

    /** @var ContactRepositoryInterface  */
    private $addressRepository;

    /** @var ContactRepositoryInterface  */
    private $contactRepository;

    /** @var EventDispatcher */
    private $eventDispatcher;

    /** @var BidRepositoryInterface */
    private $bidRepository;

    public function __construct(
        UserRepositoryInterface $userRepository,
        AddressRepositoryInterface $addressRepository,
        ContactRepositoryInterface $contactRepository,
        EventDispatcherInterface $eventDispatcher,
        BidRepositoryInterface $bidRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->addressRepository = $addressRepository;
        $this->contactRepository = $contactRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->bidRepository = $bidRepository;
    }

    public function getAll(array $criteria, array $order, int $offset, int $limit): array
    {
        /** @var User[] $users */
        $users = $this->userRepository->findAllBy($criteria, $order, $offset, $limit);

        return $users;
    }

    public function create(Email $email): User
    {
        $this->guardIsNewEmail($email);
        $user = User::create($email);
        $this->userRepository->save($user);

        return $user;
    }

    public function createFromBid(Bid $bid, int $price): User
    {

        $this->guardIsNewEmail(new Email($bid->getEmail()));

        $user = User::create(new Email($bid->getEmail()));

        $user->changeName(new Name($bid->getLastName(), $bid->getFirstName()));
        $user->changeAge($bid->getAge());
        $user->addPhone(new Phone($bid->getPhone()));
        $user->disableActive();
        $bid->accept();

        $this->userRepository->save($user);
        $this->bidRepository->save($bid);


        $this->eventDispatcher->dispatch(CreateFromBidEvent::class, new CreateFromBidEvent($user, $price));

        return $user;
    }

    public function sendPassword(int $uid): bool
    {
        $user = $this->userRepository->findOneByFields(['id' => $uid]);

        $password = (string)mt_rand(1000, 99999999);

        $user->setPassword($password);
        $user->activate();

        $this->userRepository->update($user);

        $this->eventDispatcher->dispatch(UserSendPasswordEvent::class, new UserSendPasswordEvent($user, $password));

        return true;
    }


    private function guardIsNewEmail(Email $email) : void
    {
        try{
            $this->userRepository->findOneByFields(['email' => (string)$email]);
            throw new DomainException("Пользователь с таким email уже есть");
        }catch (NotFoundException $e){
            return;
        }
    }



}
<?php

namespace SimpleDev\Learning\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use SimpleDev\Learning\Model\Lesson;

/**
 * Class LessonRepository
 * @package SimpleDev\Learning\Repository
 * @ORM\Table(name="learning_lessons")
 */
class LessonRepository extends ServiceEntityRepository implements LessonRepositoryInterface
{

    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Lesson::class);
        $this->manager = $manager;
    }

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function save(Lesson $lesson): Lesson
    {
        $this->manager->persist($lesson);
        $this->manager->flush();

        return $lesson;
    }

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return Lesson[]
     */
    public function getAll(array $criteria = [], array $orderBy = null, int $limit = 10, int $offset = 0): array
    {
        $lessons = parent::findBy($criteria, $orderBy, $limit, $offset);

        return $lessons;
    }

    /**
     * @param int $id
     * @param array $criteria
     * @return Lesson
     */
    public function findOne(int $id, array $criteria = []): Lesson
    {
        /** @var Lesson $lesson */
        $lesson = parent::findOneBy(['id' => $id] + $criteria);

        if($lesson == null)
            throw new NotFoundException("Урок не найден");

        return $lesson;
    }

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function update(Lesson $lesson): Lesson
    {
        $this->manager->flush();

        return $lesson;
    }

    /**
     * @param Lesson $lesson
     * @return bool
     */
    public function remove(Lesson $lesson): bool
    {
        $this->manager->remove($lesson);
        $this->manager->flush();

        return true;
    }
}
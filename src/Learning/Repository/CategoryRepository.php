<?php

namespace SimpleDev\Learning\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping as ORM;
use SimpleDev\Learning\Model\Category;

/**
 * Class CategoryRepository
 * @package SimpleDev\Learning\Repository
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @ORM\Table(name="learning_categories")
 */
class CategoryRepository extends ServiceEntityRepository implements CategoryRepositoryInterface
{
    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Category::class);
        $this->manager = $manager;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category
    {
        $this->manager->persist($category);
        $this->manager->flush();
        return $category;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function findOne(int $id): Category
    {
        /** @var Category $category */
        $category = parent::findOneBy(['id' => $id]);

        if($category == null)
            throw new NotFoundException("Категория не найдена");


        return $category;
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function remove(Category $category): bool
    {
        $this->manager->remove($category);
        $this->manager->flush();
        return true;
    }

    /**
     * @param array $criteria - find parameters
     * @param array $orderBy
     * @param int $limit
     * @param int $offset
     * @return Category[]
     */
    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array
    {
        $categories = parent::findBy($criteria, $orderBy, $limit, $offset);
        return $categories;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function update(Category $category): Category
    {
        $this->manager->flush();

        return $category;
    }
}
<?php

namespace SimpleDev\Learning\Service;

use SimpleDev\Learning\Model\Category;
use SimpleDev\Learning\Model\Lesson;
use SimpleDev\Learning\Repository\LessonRepositoryInterface;
use SimpleDev\Users\Model\User;

class LessonService
{

    private $repository;

    public function __construct(LessonRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function create(
        string $title, string $description, string $youtube, User $teacher, Category $category,
        string $preview = null, string $repository = null
    )
    {
        $lesson = Lesson::create($title, $description, $youtube, $teacher, $category, $preview, $repository);

        $this->repository->save($lesson);

        return $lesson;
    }

    public function one(int $id, array $criteria = []): Lesson
    {
        return $this->repository->findOne($id, $criteria);
    }

    public function getAll(array $criteria, array $orderBy, int $limit, int $offset): array
    {
        $lessons = $this->repository->getAll($criteria, $orderBy, $limit, $offset);

        return $lessons;
    }

    public function update(
        int $id, string $title, string $description, string $youtube, Category $category,
        string $preview = null, string $repository = null
    )
    {
        $lesson = $this->repository->findOne($id);

        $lesson
            ->changeTitle($title)
            ->changeDescription($description)
            ->changeYoutube($youtube)
            ->changeRepository($repository)
            ->changePreview($preview)
            ->changeCategory($category);

        $this->repository->update($lesson);

        return $lesson;
    }

    public function publish(int $id): Lesson
    {
        /** @var Lesson $lesson */
        $lesson = $this->repository->findOne($id);
        $lesson->changePublished();
        $this->repository->update($lesson);

        return $lesson;
    }

    public function image(int $id, string $path): Lesson
    {
        $lesson = $this->repository->findOne($id);
        $lesson->changePreview($path);
        $this->repository->update($lesson);
        return $lesson;
    }

    public function remove(int $id): bool
    {
        $lesson = $this->repository->findOne($id);
        $this->repository->remove($lesson);

        return true;
    }
}
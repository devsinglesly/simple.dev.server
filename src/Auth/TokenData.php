<?php

namespace App\Auth;


class TokenData implements \ArrayAccess
{

    private $data = [];
    private $token;
    /**
     * TokenData constructor.
     * @param string $token
     * @throws JsonWebTokenException
     */
    public function __construct(string $token)
    {
        $this->data = $this->verify($token);
        $this->token = $token;
    }

    /**
     * @param string $token
     * @return TokenData
     * @throws JsonWebTokenException
     */
    public static function get(string $token) : self
    {
        return new self($token);
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get(string $name)
    {
        return $this->data[$name];
    }

    public function toArray(): array
    {
        return $this->data;
    }

    public function isOld() : bool
    {
        return $this->data['express_out'] < time();
    }

    /**
     * @return string
     */
    public function getToken() : string
    {
        return $this->token;
    }

    public function refresh() : string
    {
        return Token::create($this->data);
    }

    /**
     * @param string $token
     * @return array
     * @throws JsonWebTokenException
     */
    private function verify(string $token): array
    {
        $token = new TokenValue($token);

        $parts = explode(Token::DELIMITER, $token);

        if(count($parts) !== 3 || $parts[1] === '')
        {
            throw new JsonWebTokenException("Incorrect token format");
        }

        $data = Token::decode($parts[1]);

        if($parts[2] !== Token::signature($data))
        {
            throw new JsonWebTokenException("Token in not trusted");
        }

        return $data;
    }


    /**
     * Whether a offset exists
     * @link https://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve
     * @link https://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if(isset($this->data[$offset]))
        {
            return $this->data[$offset];
        }

        return null;
    }

    /**
     * Offset to set
     * @link https://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws JsonWebTokenException
     */
    public function offsetSet($offset, $value)
    {
        throw new JsonWebTokenException("Change data token property restricted");
    }

    /**
     * Offset to unset
     * @link https://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     * @throws JsonWebTokenException
     */
    public function offsetUnset($offset)
    {
        throw new JsonWebTokenException("Change data token property restricted");
    }


    public function __debugInfo()
    {
        return $this->data;
    }
}
<?php

namespace SimpleDev\Learning\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Category
 * @package SimpleDev\Learning\Model
 * @ORM\Entity(repositoryClass="SimpleDev\Learning\Repository\CategoryRepository")
 * @ORM\Table(name="learning_categories")
 */
class Category
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", length=11)
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $preview;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isPublished = false;


    public static function create(string $title, string $description, string $preview = null): self
    {
        $category = (new self)
                    ->changePreview($preview)
                    ->changeDescription($description)
                    ->changeTitle($title);

        return $category;
    }

    public function changeTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function changeDescription(string $description):self
    {
        $this->description = $description;

        return $this;
    }

    public function changePreview(?string $preview): self
    {
        $this->preview = $preview;

        return $this;
    }

    public function changePublished(): self
    {
        $this->isPublished = !$this->isPublished;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getPreview(): ?string
    {
        return $this->preview;
    }

    /**
     * @return bool
     */
    public function isPublished(): bool
    {
        return $this->isPublished;
    }

}
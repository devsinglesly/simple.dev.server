<?php
/** @var \SimpleDev\Users\Event\UserSendPasswordEvent $event */
?>
<div style="max-width: 640px;">
    <h3>Добрый день!</h3>
    <p>

        <?= $event->user->getFirstName(); ?>, ваши данные для авторизации.
    </p>
    <hr>
    <ol style="margin-left: 15px; padding: 0;">
        <ul style="margin-left: 10px; padding: 0;">
            <li>
                <div style="display: inline-block">Логин: </div>
                <div style="display: inline-block"><?= $event->user->getEmail(); ?></div>
            </li>
            <li>
                <div style="display: inline-block">Пароль: </div>
                <div style="display: inline-block"><?= $event->password; ?></div>
            </li>
        </ul>
    </ol>

</div>





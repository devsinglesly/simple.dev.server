<?php

namespace SimpleDev\Courses\Repository;


use App\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use SimpleDev\Courses\Model\Lesson;

/**
 * @method Lesson|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lesson|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lesson[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LessonRepository extends ServiceEntityRepository implements LessonRepositoryInterface
{

    private $manager;

    public function __construct(ManagerRegistry $registry, ObjectManager $manager)
    {
        parent::__construct($registry, Lesson::class);
        $this->manager = $manager;
    }

    /**
     * @param array $criteria
     * @return Lesson[]
     */
    public function findByParams(array $criteria): array
    {
        $lessons = parent::findBy($criteria);

        return $lessons;
    }

    /**
     * @return Lesson[]
     */
    public function findAll(): array
    {
        return parent::findAll();
    }

    /**
     * @param int $id
     * @return Lesson
     */
    public function findById(int $id): Lesson
    {
        /** @var Lesson|null $lesson */
        $lesson = $this->findOneBy(['id' => $id]);
        if(!$lesson){
            throw new NotFoundException("Урок не найден");
        }
        return $lesson;
    }

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function save(Lesson $lesson): Lesson
    {
        $this->manager->persist($lesson);
        $this->manager->flush();
        return $lesson;
    }

    /**
     * @param Lesson $lesson
     * @return Lesson
     */
    public function update(Lesson $lesson): Lesson
    {
        $this->manager->flush();
        return $lesson;
    }

    /**
     * @return bool
     */
    public function remove(Lesson $lesson): bool
    {
        $this->manager->remove($lesson);
        $this->manager->flush();
        return true;
    }


}
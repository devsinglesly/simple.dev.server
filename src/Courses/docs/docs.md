# Course service

###constructor arguments
```php

    public function __construct(CourseRepositoryInterface $courseRepository, EventDispatcherInterface $eventDispatcher);

```

###get instance
```php
    $courseRepository = new CourseRepository();
    $eventDispatcher = new EventDispatcher();
    
    $courseService = new CourseService($courseRepository, $eventDispatcher);
```

###Methods

#### getPublished() : array
return only published courses
```php
    /** @var Course[] $courses */
    $courses = $courseService->getPublished();
```

#### getAll()
return course array
```php
    /** @var Course[] $courses */
    $courses = $courseService->getAll();
```

#### getById(int $id)
return single course by id
```php
    /** @var Course $course */
    $course = $courseService->getById(2);
```

#### create(string $title, string $description, string $preview, Price $price)
Create new course
return created course
```php
    /** @var Course $course */
    $title = "Beginner php course";
    
    $description = "Its awesome";
    
    /** use link on image */
    $preview = "http://image.com/some.png";
    
    $price = new Price(9500);
    
    /** @var Course $course */
    $course = $courseService->create($title, $description, $preview, $price);
```


#### edit(int $id, string $title, string $description, string $preview)
Edit course by id
return edited course
```php
    /** course id to edit */
    $id = 1;
    
    $title = "Beginner php course";
    
    $description = "Its awesome";
    
    /** use link on image */
    $preview = "http://image.com/some.png";
    
    /** @var Course $course */
    $course = $courseService->edit($id, $title, $description, $preview);
```


#### changePrice(int $id, Price $price)
Change course price by id
return course with new price
```php
    /** course id to change price */
    $id = 1;
    
    /** new course price */
    $price = new Price(2540);
    
    /** @var Course $course */
    $course = $courseService->changePrice($id, $price);
```


#### publish(int $id)
Publishing course by id
return course with new publish status
```php
    /** course id to publish */
    $id = 1;
    
    /** @var Course $course */
    $course = $courseService->publish($id);
```


#### hide(int $id)
hide publish course by id
return course with new publish status
```php
    /** course id to hide publish */
    $id = 1;
        
    /** @var Course $course */
    $course = $courseService->hide($id);
```

#### remove(int $id)
remove course by id
return boolean
```php
    /** course id to remove */
    $id = 1;
        
    /** @var boolean $course */
    $status = $courseService->remove($id);
```


---
# Lesson service


###constructor arguments
```php

    public function __construct(LessonRepositoryInterface $lessonRepository, EventDispatcherInterface $eventDispatcher);

```

###get instance
```php
    $lessonRepository = new LessonRepository();
    $eventDispatcher = new EventDispatcher();
    
    $lessonService = new LessonService($lessonRepository, $eventDispatcher);
```

###Methods

####  getAllByCourseId(int $course_id) : array
```php
    /** @var Lesson[] $lessons */
    $lessons = $lessonService->getAllByCourseId($course_id);
```

#### getAll()
return lessons array
```php
    /** @var Lesson[] $lessons */
    $lessons = $lessonService->getAll();
```

#### getById(int $id)
return single lesson by id
```php
    /** @var Lesson $lesson */
    $lesson = $lessonService->getById(2);
```

#### create(string $title, string $description, string $attachment, string $preview, Course $course)
Create new lesson
return created lesson
```php
    $title = "Beginner php lesson 1";
    
    $description = "Its awesome";
    
    /** use link for lesson materials */
    $attachemnt = "http://archive.com/lesson_archive.rar";
    
    /** use link for image */
    $preview = "http://image.com/some.png";
    
    /** @var Course $course */
    $course = $courseService->getById(4);
    
    /** @var Lesson $lesson */
    $lesson = $lessonService->create($title, $description, $attachemnt, $preview);
```


#### edit(int $id, string $title, string $description, string $preview): Lesson
Edit lesson by id
return edited lesson
```php
    /** lesson id to edit */
    $id = 1;
    
    $title = "Beginner php lesson 1";
    
    $description = "Its awesome";
    
    /** use link on image */
    $preview = "http://image.com/some.png";
    
    /** @var Lesson $lesson */
    $lesson = $lessonService->edit($id, $title, $description, $preview);
```

#### remove(int $id)
remove lesson by id
return boolean
```php
    /** lesson id to remove */
    $id = 1;
        
    /** @var boolean $course */
    $status = $lessonService->remove($id);
```

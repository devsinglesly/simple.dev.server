<?php

namespace App\Service;

use App\Entity\File;
use App\Repository\FileRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\File\File as CoreFile;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\KernelInterface;

class FileService
{
    const UPLOAD_DIR = '/public/uploads';

    private $repository;
    private $kernel;
    private $manager;

    public function __construct(FileRepository $repository, ObjectManager $manager, KernelInterface $kernel)
    {
        $this->repository = $repository;
        $this->kernel = $kernel;
        $this->manager = $manager;
    }

    public function save(UploadedFile $file) : File
    {
        $fileName = md5(uniqid()) . "." . $file->getClientOriginalExtension();
        $target = $this->getTargetDir();

        $uploadedFile = $file->move($target, $fileName);

        $entityFile = new File($uploadedFile);

        $this->manager->persist($entityFile);

        $this->manager->flush();

        return $entityFile;
    }

    /**
     * @param string $hash
     * @return File
     * @throws \App\Exception\NotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function get(string $hash) : File
    {
        $file = $this->repository->findOneByHash($hash);

        return $file;
    }

    /**
     * @param string $hash
     * @return CoreFile
     * @throws \App\Exception\NotFoundException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getResource(string $hash) : CoreFile
    {
        $file = $this->repository->findOneByHash($hash);

        $coreFile = new CoreFile($file->getFullPath());

        return $coreFile;
    }

    private function getTargetDir():string
    {
        return $this->getUploadDir() . $this->generateStructure();
    }

    private function getUploadDir():string
    {
        return $this->kernel->getProjectDir() . self::UPLOAD_DIR;
    }

    private function generateStructure(): string
    {
        list($y,$m,$d) = [date('Y'), date('m'), date('d')];
        return "/{$y}/{$m}/{$d}";
    }
}
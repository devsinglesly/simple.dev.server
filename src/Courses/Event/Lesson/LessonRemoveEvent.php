<?php

namespace SimpleDev\Courses\Event\Lesson;


use SimpleDev\Courses\Event\Event;
use SimpleDev\Courses\Model\Lesson;

class LessonRemoveEvent extends Event
{
    public $lesson;

    public function __construct(Lesson $lesson = null)
    {
        $this->lesson = $lesson;
    }
}